#include "helpers.h"
#include "compiler.h"
#include <stdlib.h>
#include <string.h>

void inline_warn(const sdc_io* io, ctx_type context) {
	if (context & CTX_INLINE)
		printf("W: %i:%i - Block element present inside an inline element\n",
				io->line, io->col);
}


size_t swrite(sdc_io* io, const char* s) {
	log_debug("writing '%s'\n", s);
	return fwrite(s, 1, strlen(s), io->output);
}


int str2cmp(const char* s1, const char* s2) {
	return strncmp(s1, s2, strlen(s2));
}


void backup(sdc_io* io, int bufsize, const char* pattern) {
	offset(io->input, (int)strlen(pattern) - bufsize);
}


void offset(FILE* stream, int o) {
	log_debug("offsetting by %i chars\n", o);
	if (fseek(stream, o, SEEK_CUR)) {
		puts("E: Failed to seek file");
		exit(SDC_IOFAILURE);
	}
}


void copy_until(sdc_io* io, const char* context, char end) {
	int line = io->line;
	int col = io->col;
	int ch;
	while ((ch = getc(io->input)) != end) {
		if (ch == EOF) {
			printf("E: Unexpected EOF scanning arguments for '%s' at %i:%i\n",
					context, line, col);
			exit(SDC_EOF);
		} else if (ch == '\n') {
			++io->line;
			io->col = 0;
		} else
			++io->col;
		putc(ch, io->output);
	}
}


sdc_status check_end(const sdc_io* io, int ch, ctx_type context) {
	if (ch == '}') {
		if (context != CTX_DOCUMENT) {
			if (context == CTX_CODE || context == CTX_CODEBLOCK) {
				putc(ch, io->output);
				return SDC_RELOOP;
			}
			log_debug("encountered end character, returning\n");
			// not a code context, no special behavior
			return SDC_OK;
		}
		printf("E: Unexpected '}' at %i:%i\n", io->line, io->col);
		return SDC_UNEXPECTED;
	}
	return SDC_CONTINUE;
}

