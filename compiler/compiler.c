#include "compiler.h"
#include "helpers.h"
#include <stdlib.h>
#include <string.h>


void compile_recurse(sdc_io* io, ctx_type context, const char* human_ctx) {
	int line = io->line;
	int col = io->col;
	log_debug("recursion event: %s\n", human_ctx);
	/// Compile the next block recursively
	sdc_status s = compile(io, context);
	
	/// Error handling
	switch(s) {
		case SDC_OK:
			return;
		case SDC_UNKNOWN:
			printf("E: Unknown error at %i:%i\n", io->line, io->col);
			exit(SDC_UNKNOWN);
		case SDC_BADARGS:
			printf("E: Internal error (BADARGS) at %i:%i\n",
					io->line, io->col);
			exit(SDC_UNKNOWN);
		case SDC_IOFAILURE:
			printf("E: Error reading file at %i:%i\n", io->line, io->col);
			exit(SDC_IOFAILURE);
		case SDC_UNEXPECTED:
			printf("E: While scanning '!%s' at %i:%i\n", human_ctx, line, col);
			exit(SDC_UNEXPECTED);
		case SDC_EOF:
			if (context != CTX_DOCUMENT)
				return;
			printf("E: Unexpected EOF at %i:%i\n", io->line, io->col);
			printf("E: While scanning '!%s' at %i:%i\n", human_ctx, line, col);
			exit(SDC_EOF);
		case SDC_CONTINUE:
			printf("E: Internal error (CONTINUE) at %i:%i\n",
					io->line, io->col);
			exit(SDC_UNKNOWN);
		case SDC_RELOOP:
			printf("E: Internal error (RELOOP) at %i:%i\n",
					io->line, io->col);
			exit(SDC_UNKNOWN);
	}
}


sdc_status compile(sdc_io* io, ctx_type context) {
	int ch;
	/// Read character-by-character
	while ((ch = getc(io->input)) != EOF) {
		/// Keep track of lines and columns for error reporting
		if (ch == '\n') {
			++io->line;
			io->col = 0;	
		} else {
			++io->col;
		}
		log_debug("%i:%i(%li) %i - read %c\n",
				io->line, io->col, ftell(io->input), context, ch);
		
		/// Check if this is the end of a section and possibly return
		sdc_status is_end = check_end(io, ch, context);
		if (is_end == SDC_RELOOP)
			continue;
		if (is_end != SDC_CONTINUE)
			return is_end;

		/// Handle basic single-char substitutions
		switch (ch) {
			case '!':
				break;
			case '<':
				swrite(io, "&lt;"); continue;
			case '>':
				swrite(io, "&rt;"); continue;
			case '\n':
				// reproduce newlines in codeblocks
				if (context == CTX_CODEBLOCK)
					putc('\n', io->output);
				// Add a paragraph break from two newlines
				int next = getc(io->input);
				if (next == '\n') {
					++io->line;
					swrite(io, "\n<div class=pbr></div>\n");
				} else {
					if (next != EOF)
						offset(io->input, -1);
				}
				continue;
			case '\t':
				// reproduce tabulators in codeblocks
				// otherwise use a space for more readable HTML
				if (context == CTX_CODEBLOCK)
					putc('\t', io->output);
				else
					putc(' ', io->output);
				continue;
			case '\\': // skip processing the next char, unless in a codeblock
				ch = getc(io->input);
				if (ch == '}' && (context == CTX_CODE ||
							context == CTX_CODEBLOCK)) {
					log_debug("escaped backslash, end of code\n");
					++io->col;
					return SDC_OK;
				} else {
					log_debug("escaped char, advancing\n");
					putc(ch, io->output);
					++io->col; continue;
				}
			case '{':
				printf("E: Unexpected '{' at %i:%i\n", io->line, io->col);
				return SDC_UNEXPECTED;
				// Unexpected closing braces handled in check_end
			default:
				log_debug("regular character\n");
				putc(ch, io->output);
				continue;
		}
		
		/// Get a buffer for multi-char bang-commands
		log_debug("encountered bang\n");
		char cmdbuf[CMDBUF]; char* pcmd = &cmdbuf[0];
		size_t cmdbuf_size = fread(pcmd, 1, CMDBUF, io->input);
		log_debug("command '%.*s'\n", 16, pcmd);
		
		/// If the bang is right before the end character, end without processing
		is_end = check_end(io, pcmd[0], context);
		if (is_end == SDC_RELOOP) {
			backup(io, cmdbuf_size, "}");
			putc('!', io->output); // Treat the bang as a normal char
			continue;
		}
		if (is_end != SDC_CONTINUE) {
			backup(io, cmdbuf_size, "}");
			putc('!', io->output);
			return is_end;
		}
		
		/// Figure out which command it is and handle it
		int opts; //used by image command
		if (pcmd[0] == '/') {
			backup(io, cmdbuf_size, "/");
			while (getc(io->input) != '\n') {}
		}
		else if (!str2cmp(pcmd, "bl{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "bl{");
			swrite(io, "\n<ul>");
			compile_recurse(io, CTX_LISTBULLETED, "bulleted list");
			swrite(io, "\n</ul>");
		}
		else if (!str2cmp(pcmd, "blockquote{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "blockquote{");
			swrite(io, "\n<div class=blockquote>");
			compile_recurse(io, CTX_BLOCKQUOTEBODY, "block-quote body");
			swrite(io, "\n</div>");
			ch = getc(io->input);
			if (ch != '{') {
				printf("W: %i:%i - Missing author from block quote\n",
						io->line, io->col);
				backup(io, cmdbuf_size, "{");
			} else {
				swrite(io, "<div class=blockquoteauthor>— ");
				compile_recurse(io, CTX_BLOCKQUOTEAUTHOR,
						"block-quote author");
				swrite(io, "\n</div>");
			}
		}
		else if (!str2cmp(pcmd, "bold{")) {
			backup(io, cmdbuf_size, "bold{");
			swrite(io, "<b>");
			compile_recurse(io, CTX_BOLD, "bolded text");
			swrite(io, "</b>");
		}
		else if (!str2cmp(pcmd, "center{")) {
			backup(io, cmdbuf_size, "center{");
			swrite(io, "<div class=center>");
			compile_recurse(io, CTX_CENTER, "center-aligned text");
			swrite(io, "</div>");
		}
		else if (!str2cmp(pcmd, "code{")) {
			backup(io, cmdbuf_size, "code{");
			swrite(io, "<span class=code>");
			compile_recurse(io, CTX_CODE, "inline code");
			swrite(io, "</span>");
		}
		else if (!str2cmp(pcmd, "codeblock{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "codeblock{");
			swrite(io, "\n<div class=code><pre>");
			compile_recurse(io, CTX_CODEBLOCK, "code block");
			swrite(io, "\n</pre></div>");
		}
		else if (!str2cmp(pcmd, "h1{")) {
			backup(io, cmdbuf_size, "h1{");
			swrite(io, "\n<h2>");
			compile_recurse(io, CTX_HEADING, "heading");
			swrite(io, "</h2>");
		}
		else if (!str2cmp(pcmd, "h2{")) {
			backup(io, cmdbuf_size, "h2{");
			swrite(io, "\n<h3>");
			compile_recurse(io, CTX_HEADING, "heading");
			swrite(io, "</h3>");
		}
		else if (!str2cmp(pcmd, "h3{")) {
			backup(io, cmdbuf_size, "h3{");
			swrite(io, "\n<h4>");
			compile_recurse(io, CTX_HEADING, "heading");
			swrite(io, "</h4>");
		}
		else if (!(opts=str2cmp(pcmd, "image{")) ||
				!str2cmp(pcmd, "image(")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "image{");
			swrite(io, "\n<figure><img ");
			if (opts) {
				swrite(io, "style=\"width:");
				copy_until(io, "image width", ',');
				swrite(io, "; height:");
				copy_until(io, "image height", ')');
				swrite(io, "\" ");
				getc(io->input);
			}
			swrite(io, "src=\"");
			copy_until(io, "image source", '}');
			ch = getc(io->input);
			if (ch == '{') {
				swrite(io, "\">\n<figcaption>");
				compile_recurse(io, CTX_IMAGECAPTION, "image caption");
				swrite(io, "</figcaption>");
			} else {
				swrite(io, "\">");
				offset(io->input, -1);
			}
			swrite(io, "\n</figure>");
		}
		else if (!str2cmp(pcmd, "italic{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "italic{");
			swrite(io, "<i>");
			compile_recurse(io, CTX_ITALIC, "italics");
			swrite(io, "</i>");
		}
		else if (!str2cmp(pcmd, "li{")) {
			backup(io, cmdbuf_size, "li{");
			swrite(io, "\n<li>");
			if (context != CTX_LISTBULLETED && context != CTX_LISTNUMBERED)
				printf("W: %i:%i - List item present outside a list "
						"container\n", io->line, io->col);
			compile_recurse(io, CTX_LISTITEM, "list item");
			swrite(io, "</li>");
		}
		else if (!str2cmp(pcmd, "link{")) {
			backup(io, cmdbuf_size, "link{");
			swrite(io, "<a href=");
			long int offset = ftell(io->input);
			copy_until(io, "link URL", '}');
			swrite(io, ">");
			ch = getc(io->input);
			if (ch != '{') {
				if (fseek(io->input, offset, SEEK_SET)) {
					puts("E: Failed to seek input file");
					exit(SDC_IOFAILURE);
				}
			}
			compile_recurse(io, CTX_LINK, "link text");
			swrite(io, "</a>");
		}
		else if (!str2cmp(pcmd, "nl{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "nl{");
			swrite(io, "\n<ol>");
			compile_recurse(io, CTX_LISTNUMBERED, "numbered list");
			swrite(io, "\n</ol>");
		}
		else if (!str2cmp(pcmd, "overlay(")) {
			log_debug("overlay\n");
			inline_warn(io, context);
			backup(io, cmdbuf_size, "overlay(");
			swrite(io, "\n<div style=\"position:fixed;left:");
			copy_until(io, "overlay(", ',');
			swrite(io, ";top:");
			copy_until(io, "overlay([...],", ')');
			swrite(io, "\">");
			getc(io->input);
			compile_recurse(io, CTX_OVERLAY, "overlain block");
			swrite(io, "</div>");
		}
		else if (!str2cmp(pcmd, "position(")) {
			log_debug("position\n");
			inline_warn(io, context);
			backup(io, cmdbuf_size, "position(");
			swrite(io, "\n<div style=\"position:absolute;left:");
			copy_until(io, "position(", ',');
			swrite(io, ";top:");
			copy_until(io, "position([...],", ')');
			swrite(io, "\">");
			getc(io->input); // discard opening brace
			compile_recurse(io, CTX_POSITION, "positioned block");
			swrite(io, "</div>");
		}
		else if (!str2cmp(pcmd, "row{")) {
			log_debug("table row\n");
			backup(io, cmdbuf_size, "row{");
			if (context != CTX_TABLE)
				printf("W: %i:%i - Table row present outside a table\n",
						io->line, io->col);
			swrite(io, "\n<tr class=table>");
			while (1) {
				swrite(io, "<td class=table>");
				compile_recurse(io, CTX_TABLECELL, "table cell");
				swrite(io, "</td>");
				ch = getc(io->input);
				if (ch != '{') {
					offset(io->input, -1);
					break;
				}
			}
			swrite(io, "</tr>");
		}
		else if (!str2cmp(pcmd, "rowheader{")) {
			log_debug("table row header\n");
			backup(io, cmdbuf_size, "rowheader{");
			if (context != CTX_TABLE)
				printf("W: %i:%i - Table row present outside a table\n",
						io->line, io->col);
			swrite(io, "\n<tr class=header>");
			while (1) {
				swrite(io, "<td class=header>");
				compile_recurse(io, CTX_TABLECELLHEADER,
						"table header cell");
				swrite(io, "</td>");
				ch = getc(io->input);
				if (ch != '{') {
					offset(io->input, -1);
					break;
				}
			}
			swrite(io, "</tr>");
		}
		else if (!str2cmp(pcmd, "s{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "s{");
			swrite(io, "<em>");
			compile_recurse(io, CTX_SLANT, "slant");
			swrite(io, "</em>");
		}
		else if (!str2cmp(pcmd, "section{")) {
			log_debug("section\n");
			inline_warn(io, context);
			backup(io, cmdbuf_size, "section{");
			swrite(io, "\n<details open><summary>");
			compile_recurse(io, CTX_SECTIONTITLE, "section title");
			swrite(io, "</summary>");
			ch = getc(io->input);
			if (ch != '{') {
				swrite(io, "</details>");
				printf("W: %i:%i - Missing body from section\n",
						io->line, io->col);
				offset(io->input, -1);
			} else {
				swrite(io, "<div class=section>\n");
				compile_recurse(io, CTX_SECTIONBODY, "section body");
				swrite(io, "\n</div></details>");
			}
		}
		else if (!str2cmp(pcmd, "strike{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "strike{");
			swrite(io, "<s>");
			compile_recurse(io, CTX_STRIKE, "strikethrough");
			swrite(io, "</s>");
		}
		else if (!str2cmp(pcmd, "subtitle{")) {
			backup(io, cmdbuf_size, "subtitle{");
			swrite(io, "\n<h3 class=subtitle>");
			compile_recurse(io, CTX_SUBTITLE, "subtitle");
			swrite(io, "</h3>");
		}
		else if (!str2cmp(pcmd, "table(")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "table(");
			swrite(io, "\n<table class=\"table ");
			copy_until(io, "table(", ')');
			swrite(io, "\">");
			ch = getc(io->input);
			if (ch != '{') {
				printf("W: %i:%i - Table has no body\n",
						io->line, io->col);
				offset(io->input, -1);
			} else
				compile_recurse(io, CTX_TABLE, "table");
			swrite(io, "\n</table>");
		}
		else if (!str2cmp(pcmd, "title{")) {
			backup(io, cmdbuf_size, "title{");
			swrite(io, "\n<h1>");
			compile_recurse(io, CTX_TITLE, "title");
			swrite(io, "</h1><hr class=h1>");
		}
		else if (!str2cmp(pcmd, "u{")) {
			inline_warn(io, context);
			backup(io, cmdbuf_size, "u{");
			swrite(io, "<u>");
			compile_recurse(io, CTX_UNDERLINE, "underline");
			swrite(io, "</u>");
		}
		else if (pcmd[0] == 'b') {
			log_debug("linebreak\n");
			backup(io, cmdbuf_size, "b");
			if (context == CTX_CODEBLOCK)
				swrite(io, "!b");
			else
				swrite(io, "<br>\n");
		}
		else if (pcmd[0] == 't') {
			log_debug("tabulator\n");
			backup(io, cmdbuf_size, "t");
			swrite(io, "<span class=indent></span>");
		}
		else {
			printf("W: %i:%i - Command not recognized\n", io->line, io->col);
			backup(io, cmdbuf_size, "!");
			putc('!', io->output);
			putc(cmdbuf[0], io->output);
		}
	} // end read loop
	return SDC_EOF;
}

