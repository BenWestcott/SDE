#ifndef SDC_HELPERS_H
#define SDC_HELPERS_H

#include "compiler.h"

/// Emit a warning if the context is inline
void inline_warn(const sdc_io* io, ctx_type context);

/// Wrapper for fwrite
size_t swrite(sdc_io* io, const char* s);

/// strncmp on the length of the second string
int str2cmp(const char* s1, const char* s2);

/// Rewind the input stream by the number of unprocessed chars in the buffer
void backup(sdc_io* io, int bufsize, const char* pattern);

/// Offset the given stream cursor by o bytes
void offset(FILE* stream, int o);

/** Char-by-char copy input to output until the specified char is encountered.
 * The end character is discarded.
 */
void copy_until(sdc_io* io, const char* context, char end);

/** Check if the current character matches the criteria to end this block
 * @return SDE_RELOOP if inside a codeblock and not the end (char will be written to output)
 * @return SDC_OK if this is the end of this block
 * @return SDC_UNEXPECTED if an end was detected but this is the top-level document
 * @return SDC_CONTINUE if this is not the end of the block
 */
sdc_status check_end(const sdc_io* io, int ch, ctx_type context);

#endif
