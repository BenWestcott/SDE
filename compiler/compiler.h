#pragma once
#ifndef SDC_COMPILER_H
#define SDC_COMPILER_H

#include <stdio.h>
#include <stddef.h>

#define CMDBUF 16
#define ARGBUF 16
//#define DEBUG


#ifdef DEBUG
	#define log_debug(...) printf("DEBUG: "__VA_ARGS__)
#else
	#define log_debug(...)
#endif


typedef struct {
	char* start;
	FILE* input;
	FILE* output;
	int line;
	int col;
} sdc_io;


typedef enum {
	SDC_OK = 0,
	SDC_UNKNOWN = 1,
	SDC_BADARGS = 2,
	SDC_IOFAILURE = 3,
	SDC_UNEXPECTED = 4,
	SDC_EOF = 5,
	SDC_CONTINUE = 6,
	SDC_RELOOP = 7
} sdc_status;


#define CTX_INLINE (1 << 7)
typedef enum {
	CTX_DOCUMENT         = 0,
	CTX_BOLD             = 1  | CTX_INLINE,
	CTX_BLOCKQUOTEAUTHOR = 2  | CTX_INLINE,
	CTX_BLOCKQUOTEBODY   = 3,
	CTX_CENTER           = 4  | CTX_INLINE,
	CTX_CODE             = 5  | CTX_INLINE,
	CTX_CODEBLOCK        = 6,
	CTX_HEADING          = 7  | CTX_INLINE,
	CTX_IMAGECAPTION     = 8  | CTX_INLINE,
	CTX_ITALIC           = 9  | CTX_INLINE,
	CTX_LINK             = 10 | CTX_INLINE,
	CTX_LISTBULLETED     = 11,
	CTX_LISTITEM         = 12 | CTX_INLINE,
	CTX_LISTNUMBERED     = 13,
	CTX_OVERLAY          = 14,
	CTX_POSITION         = 15,
	CTX_SECTIONTITLE     = 16 | CTX_INLINE,
	CTX_SECTIONBODY      = 17,
	CTX_SLANT            = 18 | CTX_INLINE,
	CTX_STRIKE           = 19 | CTX_INLINE,
	CTX_SUBTITLE         = 20 | CTX_INLINE,
	CTX_TABLE            = 21,
	CTX_TABLECELL        = 22 | CTX_INLINE,
	CTX_TABLECELLHEADER  = 23 | CTX_INLINE,
	CTX_TITLE            = 24 | CTX_INLINE,
	CTX_UNDERLINE        = 25 | CTX_INLINE
} ctx_type;



/// Compiler init
void init(sdc_io* io, int argc, char** argv);

/** Reads, parses, and writes.
 * @param context Type of the container element 
 */
sdc_status compile(sdc_io* io, ctx_type context);

/** Performs a recursive call of compile() and error-checks the return value.
 * @param context Name of the container element
 * @param human_ctx Human-readable string form of the context
 */
void compile_recurse(sdc_io* io, ctx_type context, const char* human_ctx);

#endif

