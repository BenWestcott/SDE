## Bugs
- Table noborder style does not work
- Separate table styles with commas instead of spaces
## Features
- Templates (wiki, paper, more?) - also use as magic number
- Page numbers
- Colspan and rowspan table cell attributes (how?)
- Rendered TeX expressions
- Multi-line comments
- A real function argument parser
- Clickable breadcrumbs in the document bar
- Embed the document source in the final HTML and add a utility for re-rendering
- Try to read external CSS and fall back to embedded
- Figure out elegant syntax for adding alt-text to an image
## Drawables
- TODO element, displayed in high-contrast and prints a warning to STDOUT
- Superscript, subscript
- Author
- Endnotes
- Sidebar (left/right, settable width)
- Table of contents

